%%
% Script for single-cell analysis from Gray, A. CpoB paper
% Run in directory with all *_DATA.mat morphometrics output files
% This is in cell format; run the script one section at a time
% Related scripts for measurement of periplasmic leakage (peripheral
% fluorescence) and outer membrane blebs/vesicles (fluorescent puncta).
% Scripts rely on custom MATLAB software called Morphometrics.
% This software is currently in beta version and freely available upon
% request (kchuang@stanford.edu); the release version will be
% made available on Bitbucket.
% For any questions regarding this script, please email
% Alexandre Colavin at acolavin@stanford.edu
                                    
clear all; close all; clc;
%% get all contour files from morphometrics
filenames = FindAllTiffsSubDir(pwd,'*RS.mat');
%% get strain names
strains = struct([]);
strainnames = {};
straininds = [];
% for each strain here, we're just prepping the data structure
for l = 1:length(filenames)
    [~,curname,~] = fileparts(filenames{l});
    underscores = find(curname=='_');
    curname = curname(1:(underscores(1)-1));
    
    curind = find(ismember(strainnames,curname));
    
    if isempty(curind)
        strainnames{end+1} = curname;
        curind = length(strainnames);
        strains(curind).name = curname;
        strains(curind).cell = struct([]);
    end
    
    straininds(l) = curind;
    
end
%% Remove fields of view that are noisy (indicating fluorescence artifacts)
for k = 1:length(strains)
    fprintf('Strain %i of %i.\n',k,length(strains));
    curls = find(straininds == k);
    strains(k).cell = struct([]);
    strains(k).numcells = 0;
    strains(k).puncta = struct([]);
    strains(k).filenames = {filenames{curls}};
    removecurls = zeros(1,length(strains(k).filenames));
    for l = 1:length(curls)
%         cla; hold on;
        [~,name,~] = fileparts(filenames{l});
        unders = find(name=='_');
        phasename = [name(1:(unders(1)-1)),'/',name(1:end-14),'fluor.tif'];
        a = double(imread(phasename));
%         imshow(a); hold on;
% 
        load([name(1:(unders(1)-1)),'/',name,'.mat'])
        for c = 1:length(frame.object)
            plot(frame.object(c).Xcont([1:end 1]),frame.object(c).Ycont([1:end 1]),'r');
        end
        disp(nnz(a>62000)/numel(a));
        if nnz(a>62000)/numel(a) < .005
            removecurls(l) = 0;
        end
    end
    nnz(removecurls)
    strains(k).filenames(find(removecurls)) = [];

end
%% Now filter and record width/length for each cell in each file
muperpixel = .0644;
curvature_thresh = .15;
warning('off','signal:findpeaks:largeMinPeakHeight');
for k = 1:length(strains)
    fprintf('Strain %i of %i.\n',k,length(strains));
    
    for l = 1:length(strains(k).filenames)
        name = strains(k).filenames{l};
        
        load(name)
        [~,filename,exten] = fileparts(name);
        underscores = find(filename == '_');
        dirname = filename(1:(underscores(1)-1));
        fluorim = double(imread([dirname,'/',filename(1:end-15),'_fluor.tif']));
        
        close all;
        if strcmp(strains(k).name,'pal');
            frame = frame(1);
        end
        for o = 1:length(frame.object)
            [frame.object(o).mesh,frame.object(o).cell_length,frame.object(o).cell_width] = calculate_mesh(frame.object(o));
        end
        % Now remove all cells that don't pass muster
        frame = findpuncta(fluorim,frame,toplot);
        clear cell

        strains(k).numcells = strains(k).numcells + length(frame.object);
        for g = 1:length(frame.object)
            frame.object(g).on_edge=0;
            if cell_filter(frame.object(g),curvature_thresh,-1*curvature_thresh);
                frame.object(g).filenum = l;

                if isempty(strains(k).cell)
                    strains(k).cell = frame.object(g);
                else
                    strains(k).cell(end+1) = frame.object(g);
                end
            end
        end
        for p = 1:length(frame.puncta)
            frame.puncta(p).filenum = l;
            if isempty(strains(k).puncta)
                strains(k).puncta = frame.puncta(p);
            else
                strains(k).puncta(end+1) = frame.puncta(p);
            end
        end
        strains(k).numpuncta(l) = length(frame.puncta);
    end
    % remove puncta information for for frames with more than 3x deviation
    % from mean.
    threshnum = median(strains(k).numpuncta)+3*std(strains(k).numpuncta);
    toremove = find(strains(k).numpuncta>threshnum);
    strains(k).puncta(ismember(cell2mat({strains(k).puncta.filenum}),toremove)) = [];
    strains(k).cell(ismember(cell2mat({strains(k).cell.filenum}),toremove)) = [];
    

end
%% small graphs for manuscript
figure;
strains2plot = {'WT1','lpoA1','ybgF1','ybgFlpoA1','mrcB1'};%'mrcB-ybgF'};
minthresh = 700;
amountbelow = zeros(1,length(strains2plot));
colors = jet(length(strains2plot));
strainnames = {strains.name};
strainlegend = strains2plot;
for l = 1:length(strains2plot)
    curstrain = find(strcmp(strains2plot{l},strainnames));
    subplot(3,1,1); hold on;
    [n,x] = hist(cellfun(@median,{strains(curstrain).cell.fluorext}),linspace(400,1800,30));
    plot(x,n/sum(n)/diff(x(1:2)),'Color',colors(l,:),'LineWidth',2);
    amountbelow(l) = nnz(cellfun(@median,{strains(curstrain).cell.fluorext})<minthresh)/length(strains(curstrain).cell);
    

    subplot(3,1,2); hold on; 
    strainlegend{l} = [strainlegend{l} ' (', num2str(sum(n)),')'];
    bar(l,amountbelow(l),'FaceColor',colors(l,:));
    subplot(3,1,3); hold on; 
    bar(l,median(cellfun(@median,{strains(curstrain).cell.fluorext})),'FaceColor',colors(l,:));
end
subplot(3,1,1);
legend(strainlegend); xlim([450,1800]); 
xlabel('Fluorescence (a.u.)'); ylabel('Fraction of cells');
subplot(3,1,2);
xlim([0,length(strains2plot)+1]);
set(gca,'XTick',[1:length(strains2plot)]);
set(gca,'XTickLabel',strains2plot);
ylabel('Fraction below threshold');
subplot(3,1,3);
xlim([0,length(strains2plot)+1]);
set(gca,'XTick',[1:length(strains2plot)]);
set(gca,'XTickLabel',strains2plot);
ylabel('Median Fluorescence');

%% Quantify blebs
figure('Color',[1,1,1]);
if exist('strains2','var');
    strains = strains2;
else
    strains2 = strains;
end


strains = strains(find(ismember(strainnames,strains2plot)));
blebs = {};
blebint = {};
vesicles = {};
cells = [];
cells2 = [];
for l =1 :length(strains);
    blebs{end+1} = find(cell2mat({strains(l).puncta.nearcell}));

    blebint{end+1} = cellfun(@median,{strains(l).puncta.pxlval});
    vesicles{end+1} = find(~cell2mat({strains(l).puncta.nearcell}));
    cells(end+1) = length(strains(l).cell);
    cells2(end+1) = strains(l).numcells;
end

blebNearCell = cellfun(@length,blebs)./cells;
vesNearCell = cellfun(@length,vesicles)./cells;
subplot(1,2,1);
barh(1:length(vesNearCell),vesNearCell); ylim([0,length(vesNearCell)+1]);
set(gca,'YTick',[1:length(blebNearCell)]); set(gca,'YTickLabel',{strains.name});
xlabel('Vesicles per cell','FontSize',12);
subplot(1,2,2);
barh(1:length(blebNearCell),blebNearCell); ylim([0,length(blebNearCell)+1]);
set(gca,'YTick',[1:length(blebNearCell)]); set(gca,'YTickLabel',{strains.name});
xlabel('Blebs per cell','FontSize',12);
figure;
colors = jet(length(blebint));

for l=  1:length(blebint)
    [n,x] = hist(blebint{l},linspace(0,1e4,40));
    plot(x,n/sum(n)/diff(x(1:2)),'Color',colors(l,:)); hold on;
end
groups = cellfun(@(x,y) ones(1,length(x))*(y-1),blebint,num2cell(1:length(blebint),length(blebint)),'UniformOutput',0);
legend({strains.name})
%% Quantify vesicles (non-cell associated blebs)
vesicleNearCell = cellfun(@length,vesicles)./cells2;
figure('Color',[1,1,1]);
barh(1:length(vesicleNearCell),vesicleNearCell); ylim([0,length(blebNearCell)+1]);
set(gca,'YTick',[1:length(vesicleNearCell)]); set(gca,'YTickLabel',{strains.name});
xlabel('Fluorescence (a.u.)');

